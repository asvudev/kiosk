// ==UserScript==
// @name         VU Kiosk
// @namespace    https://vu.wwu.edu/
// @version      1.11
// @downloadURL  https://bitbucket.org/asvudev/kiosk/raw/HEAD/kiosk.user.js
// @updateURL    https://bitbucket.org/asvudev/kiosk/raw/HEAD/kiosk.meta.js
// @description  Kiosk menu wrapper
// @author       Justin Revelstoke esq.
// @require      https://cdn.rawgit.com/MachinisteWeb/konami-code-js/master/src/konami-code.js
// @match        *://*/*
// @noframes
// ==/UserScript==

(function() {
    'use strict';

    // kiosk link buttons ["LABEL", "https://urltogo.to"]
    var links = [
        ["HOME","https://vu.wwu.edu/"],
        ["VU MAP", "https://vumap.z5.web.core.windows.net/"],
        ["WWU CAMPUS MAP", "https://www.wwu.edu/map"],
        ["LOST/FOUND", "https://lostfound.wwu.edu"],
        ["HOUSING", "https://housing.wwu.edu/"],
        ["JOBS", "https://vu.wwu.edu/StudentJobs"],
        ["EMPLOYMENT", "http://www.finaid.wwu.edu/studentjobs/"],
        ["WIN", "https://win.wwu.edu/"],
    ];

    var style = document.createElement("style");
    style.innerHTML = `
@import url('https://fonts.googleapis.com/css?family=Fira+Sans&display=swap');
@import url('https://fonts.googleapis.com/icon?family=Material+Icons');
body {
  margin-bottom: 120px;
}

#kiosk-menu {
  display: flex;
  justify-content: space-evenly;
  position: fixed;
  bottom: 0;
  left: 0;
  z-index: 9999999;
  width: 100%;
  height: 120px;
  background-color: #003f87;
  border-top: solid 4px white;
  font-family: Fira Sans,sans-serif !important;
  font-size: 22px !important;
}

.kiosk-button {
  display: flex;
  height: auto;
  background-color: #bad80a;
  border-radius: 200px;
  border: white solid 3px;
  color: #003f87;
  cursor: pointer;
  margin: 30px 0;
  padding: 15px 35px;
  vertical-align: middle;
  text-decoration: none;
  text-align: center;
  width: auto;
  line-height: 1;
}

.float-nav{
  display: flex;
  height: auto;
  width: auto;
  background-color: #003f87;
  border-radius: 200px;
  border: #003f87 solid;
  color: white;
  top: 80%;
  margin: 20px;
  padding: 15px 35px;
  font-size: 3.5em;
  vertical-align: middle;
  text-decoration: none;
  text-align: center;
  position: fixed;
}

.float-nav:active{
 color: #003f87;
 background-color: white;
}

.nav-left{
 left: 0%;
}

.nav-right{
 right: 0%
}

`;
    document.head.appendChild(style);

    // Creating a div to contain both of the navigation buttons
    var nav_container = document.createElement("div");
    var back = document.createElement("div");
    var forward = document.createElement("div");

    // Creating a div to contain both forward and backward navigation buttons
    nav_container.id = "nav-container";
    back.id = "back-button"
    forward.id = "forward-button"
    // using onClick() to trigger back and forward navigation
    back.innerHTML = `<span onClick="window.history.back()" class="material-icons float-nav nav-left">west</span>`;
    forward.innerHTML = `<span onClick="window.history.forward()" class="material-icons float-nav nav-right">east</span>`;

    nav_container.appendChild(back);
    nav_container.appendChild(forward);
    document.body.appendChild(nav_container);

    var menu = document.createElement("div");
    menu.id = "kiosk-menu";
    links.forEach(function(value, index, array) {
        var btn = document.createElement("div");
        btn.innerText = value[0];
        btn.className = "kiosk-button";
        btn.onclick = function() {window.location.href = value[1]};
        menu.appendChild(btn);
    });
    document.body.appendChild(menu);

    // additional dom updates

    // hide student job file links
    var pdf = document.querySelector('[title="VU Student Employment Application PDF"]');
    if (pdf) {
        pdf.style.display = 'none';
    }
    var word = document.querySelector('[title="VU Student Employment Word Application"]');
    if (word) {
        word.style.display = 'none';
    }
    
    // prevent covering up the base document
    document.body.style.height = document.body.clientHeight + 240 + 'px';
    document.body.style.overflowY = 'auto';

    // konami code secret update
    new KonamiCode().setCallback(function(){
        window.location.href = "https://bitbucket.org/asvudev/kiosk/raw/HEAD/kiosk.user.js";
    });
})();