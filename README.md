# kiosk

This repo contains the userscript js code for the infodesk kiosk.
Designed to run in chrome using the tampermonkey extension.

### Configuring

Create a new userscript in tampermonkey. Paste in the kiosk.user.js contents. Save. Go to the settings tab on the left. 
Add https://bitbucket.org/asvudev/kiosk/raw/HEAD/kiosk.user.js as the update url. Save. Go to the settings tab on the right.
Set the internal update interval to 6 hours. Save.

### Updating

New links can be added to the links array towards the top of the .user.js script, format ["LABEL", "https://urltogo.to"],
Then bump the version number in the .meta.js file to prompt an update.

### Deploying

Edit and commit the new kiosk.user.js links in bitbucket. Tampermonkey should detect the changes and update the kiosk within 6 hours. 

### Windows OS and Chrome web browser Config

Chrome configration:  Open up Startup Apps in Windows. If you do not see Google Chrome, then it will need to be added to Startup Apps. Open file location of the Chrome.exe in a Window. Then Windows key+ R and Run shell:startup command. Copy the Chrome.exe icon from the other Window into this Window. Right-click on the icon and in the Target field use this path "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" -- start-fullscreen  or  addend ' -- start-fullscreen ' to the end of the existing path in that Target field. Now in Chrome: Settings/On Startup/Open a specific set of pages. Then enter https://vu.wwu.edu as the only homepage. Save.  Doing this will open Chrome automatically as part of the Windows login process, open Chrome in full-screen mode, and also open vu.wwu.edu automatically. Doing all of this will automatically trigger the Tampermonkey scripts to then update browser to enter "Kiosk" mode. Success! 

Windows configuration: Make sure local account 'Kiosk' with p/w 'Kiosk123' is set to auto-login at bootup. https://learn.microsoft.com/en-us/troubleshoot/windows-server/user-profiles-and-logon/turn-on-automatic-logon .  Also make sure Power & Sleep settings has 'Turn off the display' set to Never, and 'Put the computer to sleep' set to Never. The 'VU KIOSK' power profile should be set by default as it is now the only power profile available. 